import { expect } from "chai";
import { race } from "../src/myPromises/race";
import { echo } from "../src/myPromises/utils";

describe("Testing race module", () => {
    context("# race logic tests", () => {
        it("Should get the first item to resolved ", async () => {
            const query = await race([
                echo("first", 4000),
                echo("second", 1000),
                echo("third", 3000),
            ]);

            expect(query).to.be.equal("second");
        });

        it("Should be a function", () => {
            expect(race).to.be.a("function");
            expect(race).to.be.a.instanceOf(Function);
        });
    });

    context("# race error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await race()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            expect(error).to.be.equal("Error: race expects 1 input");
        });
    });
});
