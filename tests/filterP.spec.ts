import { expect } from "chai";
import { filterP } from "../src/myPromises/filterParallel";
import { random, delay } from "../src/myPromises/utils";
import log from "@ajar/marker";
import { mapP } from "../src/myPromises/mapParallel";

describe("Testing filterParallel module", () => {
    context("# filterP logic tests", () => {
        it("Should be resolved to clear english word string ", async () => {
            const query = await filterP(
                "31@p$e556r0*f#e%12c^t0o",
                async (char) => {
                    log.cyan(`${char} -->`);
                    await delay(random(2000, 500));
                    log.magenta(`<-- ${char}`);
                    return /^[A-Za-z]+$/.test(char);
                }
            );

            expect(query).to.deep.equal([
                "p",
                "e",
                "r",
                "f",
                "e",
                "c",
                "t",
                "o",
            ]);
        });

        it("Should be a function", () => {
            expect(filterP).to.be.a("function");
            expect(filterP).to.be.a.instanceOf(Function);
        });
    });

    context("# filterP error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await filterP()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: filterP expects 2 arguments");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await filterP([])
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: filterP expects 2 arguments");
        });
    });
});
