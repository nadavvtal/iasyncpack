import { expect } from "chai";
import { filterS } from "../src/myPromises/filterSeries";
import { random, delay } from "../src/myPromises/utils";
import log from "@ajar/marker";

describe("Testing filterSeries module", () => {
    context("# filterS logic tests", () => {
        it("Should be resolved to clear english word string ", async () => {
            const query = await filterS(
                "31@p$e556r0*f#e%12c^t0o",
                async (char: string) => {
                    log.cyan(`${char} -->`);
                    await delay(random(1000, 100));
                    log.magenta(`<-- ${char}`);
                    return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
                }
            );

            expect(query).to.deep.equal([
                "p",
                "e",
                "r",
                "f",
                "e",
                "c",
                "t",
                "o",
            ]);
        });

        it("Should be a function", () => {
            expect(filterS).to.be.a("function");
            expect(filterS).to.be.a.instanceOf(Function);
        });
    });

    context("# filterS error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await filterS()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: filterS expects 2 arguments");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await filterS([])
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: filterS expects 2 arguments");
        });
    });
});
