import { expect } from "chai";
import { delay } from "../src/myPromises/promisDelay";

describe("Testing PromisDelay module", () => {
    context("# delay logic test", () => {
        it("Should be a function", () => {
            expect(delay).to.be.a("function");
            expect(delay).to.be.a.instanceOf(Function);
        });
    });

    context("# delay error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await delay()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            expect(error).to.be.equal("Error: delay expects 1 input");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await delay("f")
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            expect(error).to.be.equal("Error: delay expects number");
        });
    });
});
