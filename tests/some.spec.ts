import { expect } from "chai";
import { some } from "../src/myPromises/some";
import { echo } from "../src/myPromises/utils";

describe("Testing some module", () => {
    context("# some logic tests", () => {
        it("Should get the 1 first items to resolved", async () => {
            const query = await some(
                [
                    echo("first", 4000),
                    echo("second", 400),
                    echo("third", 3000),
                    echo("forth", 3000),
                    echo("fifth", 500),
                ],
                1
            );
            expect(query).to.be.deep.equal(["second"]);
        });

        it("Should get the 2 first items to resolved", async () => {
            const query = await some(
                [
                    echo("first", 4000),
                    echo("second", 400),
                    echo("third", 3000),
                    echo("forth", 3000),
                    echo("fifth", 500),
                ],
                2
            );
            expect(query).to.be.deep.equal(["second", "fifth"]);
        });

        it("Should get the 3 first items to resolved", async () => {
            const query1 = await some(
                [
                    echo("first", 4000),
                    echo("second", 400),
                    echo("third", 3000),
                    echo("forth", 3000),
                    echo("fifth", 500),
                ],
                3
            );
            expect(query1).to.be.deep.equal(["second", "fifth", "third"]);
        });
        it("Should be a function", () => {
            expect(some).to.be.a("function");
            expect(some).to.be.a.instanceOf(Function);
        });
    });

    context("# some error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await some()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: some expects 2 inputs");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await some([])
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: some expects 2 inputs");
        });
    });
});
