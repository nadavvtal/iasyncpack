import { expect } from "chai";
import { mapS } from "../src/myPromises/mapSequence";
import { random, delay } from "../src/myPromises/utils";
import log from "@ajar/marker";

describe("Testing mapSeries module", () => {
    context("# mapS logic tests", () => {
        it("Should be resolved to Array ", async () => {
            const query = await mapS("bestpack", async (char) => {
                log.cyan(`${char} -->`);
                await delay(random(2000, 500));
                log.magenta(`<-- ${char}`);
                return char.toUpperCase(); // Modify each item in the iterable
            });
            expect(query).to.deep.equal([
                "B",
                "E",
                "S",
                "T",
                "P",
                "A",
                "C",
                "K",
            ]);
        });

        it("Should be a function", () => {
            expect(mapS).to.be.a("function");
            expect(mapS).to.be.a.instanceOf(Function);
        });
    });

    context("# mapS error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await mapS()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: mapS expects 2 arguments");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await mapS([])
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: mapS expects 2 arguments");
        });
    });
});
