import { expect } from "chai";
import { mapP } from "../src/myPromises/mapParallel";
import { echo, random, delay } from "../src/myPromises/utils";
import log from "@ajar/marker";

/**   P.mapParallel()  **/

describe("Testing mapParallel module", () => {
    context("# mapP logic tests", () => {
        const promise1 = echo("to", 1000);
        const promise2 = echo("upper", 100);
        const promise3 = echo("case", 2000);

        it("Should be resolved to Array ", async () => {
            const query = await mapP(
                [promise1, promise2, promise3],
                async (p) => {
                    const char = await p;
                    log.cyan(`${char} <--`);
                    await delay(random(2000, 500));
                    log.magenta(`<-- ${char}`);
                    return char.toUpperCase(); // Modify each item in the iterable
                }
            );

            expect(query).to.deep.equal(["TO", "UPPER", "CASE"]);
        });

        it("Should return the uppercase version of the string", async () => {
            const query = await mapP("awesome", async (char: string) => {
                log.cyan(`${char} -->`);
                await delay(random(2000, 500));
                log.magenta(`<-- ${char}`);
                return char.toUpperCase(); // Modify each item in the iterable
            });

            expect(query).to.deep.equal(["A", "W", "E", "S", "O", "M", "E"]);
        });

        it("Should be a function", () => {
            expect(mapP).to.be.a("function");
            expect(mapP).to.be.a.instanceOf(Function);
        });
    });

    context("# mapP error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await mapP()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: mapP expects 2 arguments");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await mapP([])
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: mapP expects 2 arguments");
        });
    });
});
