import { expect } from "chai";
import { props } from "../src/myPromises/props";
import { echo } from "../src/myPromises/utils";

describe("Testing props module", () => {
    context("# props logic tests", () => {
        const promise1 = echo("1 first resolved value", 3000);
        const promise2 = echo("2 second resolved value", 3000);
        const promise3 = echo("3 third resolved value", 3000);
        const ans = {
            promise1: "1 first resolved value",
            promise2: "2 second resolved value",
            promise3: "3 third resolved value",
        };
        it("Should be resolved to object", async () => {
            expect(await props({ promise1, promise2, promise3 })).to.deep.equal(
                ans
            );
        });

        it("Should be a function", () => {
            expect(props).to.be.a("function");
            expect(props).to.be.a.instanceOf(Function);
        });
    });

    context("# props error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await props()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            expect(error).to.be.equal("Error: props expects 1 input");
        });
    });
});
