import { expect } from "chai";
import exp from "constants";
import { all } from "../src/myPromises/promisAll";
import { echo, delay } from "../src/myPromises/utils";

describe("Testing PromiseAll module", () => {
    const promise1 = echo("1 first resolved value", 3000);
    const promise2 = echo("2 second resolved value", 3000);
    const promise3 = echo("3 third resolved value", 3000);
    const echoB = async (msg: string, ms: number) => {
        await delay(ms);
        return msg;
    };

    context("# Promise all logic tests", () => {
        it("Should be resolved to Array 1", async () => {
            expect(await all([promise1, promise2, promise3])).to.deep.equal([
                "1 first resolved value",
                "2 second resolved value",
                "3 third resolved value",
            ]);
        });

        it("Should be resolved to Array ", async () => {
            const a = await all([promise1, echoB("yo", 3500), promise3]);

            expect(a).to.deep.equal([
                "1 first resolved value",
                "yo",
                "3 third resolved value",
            ]);
        });

        it("Should be a function", () => {
            expect(all).to.be.a("function");
            expect(all).to.be.a.instanceOf(Function);
        });
    });
    context("# promise all error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await all()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            expect(error).to.be.equal("Error: Promise all expects arguments");
        });
    });
});
