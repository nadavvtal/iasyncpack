import { expect } from "chai";
import { reduce } from "../src/myPromises/reduce";
import { random, delay } from "../src/myPromises/utils";
import log from "@ajar/marker";

describe("Testing reduce module", () => {
    context("# reduce logic tests", () => {
        it("Should be sum of the arr ", async () => {
            const query = await reduce(
                [1, 4, 7, 9, 1],
                async (total: number, num: number) => {
                    log.cyan(`${num} -->`);
                    await delay(random(1000, 100));
                    log.magenta(`<-- ${num}`);
                    return total + num;
                },
                0
            );
            console.log(query);

            expect(query).to.be.equal(22);
        });

        it("Should be a function", () => {
            expect(reduce).to.be.a("function");
            expect(reduce).to.be.a.instanceOf(Function);
        });
    });

    context("# reduce error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await reduce()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: reduce expects 3 inputs");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await reduce([])
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: reduce expects 3 inputs");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await reduce([], () => {})
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            console.log(error);
            expect(error).to.be.equal("Error: reduce expects 3 inputs");
        });
    });
});
