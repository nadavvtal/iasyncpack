import { expect } from "chai";
import { each } from "../src/myPromises/forEach";
import { delay, random } from "../src/myPromises/utils";
import log from "@ajar/marker";

describe("Testing forEach module", async () => {
    context("#each logic tests", async () => {
        it("Should be resolved to object", async () => {
            const query: any = await each("Geronimo", async (char: string) => {
                log.cyan(`${char} -->`);
                await delay(random(2000, 500));
                log.magenta(`<-- ${char}`);
                return char.toUpperCase();
            });

            const ans = ["G", "E", "R", "O", "N", "I", "M", "O"];
            expect(query).to.deep.equal(ans);
        });

        it("Should be a function", () => {
            expect(each).to.be.a("function");
            expect(each).to.be.a.instanceOf(Function);
        });
    });

    context("# each error handling", () => {
        it("should throw: expects arguments", async () => {
            let error = "";
            await each()
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            expect(error).to.be.equal("Error: each expects 2 arguments");
        });

        it("should throw: expects arguments", async () => {
            let error = "";
            await each([])
                .then(() => {
                    error = "problem with error handling";
                })
                .catch((err) => {
                    error = err.message;
                });
            expect(error).to.be.equal("Error: each expects 2 arguments");
        });
    });
});
