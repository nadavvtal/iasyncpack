<h1 align="center">Welcome to ipackasync 👋</h1>
[![ts](https://badgen.net/badge/-/node?icon=typescript&label&labelColor=blue&color=555555)](https://github.com/TypeStrong/ts-node)
[<img src="https://img.shields.io/badge/-ESLint-4B32C3.svg?logo=ESlint">](https://eslint.org/)
[![Husky](https://img.shields.io/static/v1?label=Formated+by&message=Prettier&color=ff69b4)](https://github.com/prettier/prettier)
[![Husky](https://img.shields.io/static/v1?label=Hooked+by&message=Husky&color=success)](https://typicode.github.io/husky)
 ![ts](https://badgen.net/badge/-/TypeScript/blue?icon=typescript&label)

<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.1.2-blue.svg?cacheSeconds=2592000" />
</p>

> Type Script project to handle async operation and operations with Promise objects  and itrerable Promise objects.

### 🏠 [Homepage](https://www.npmjs.com/package/@nadavtal/asyncpack)

## Install

```sh
npm install @nadavtal/iasyncpack
```

## Usage

```sh
import [choose-modu] from @nadavtal/iasyncpack
```

## Run tests

```sh
npm run tests
```

## Run coverage(istanbul)

```sh
npm run coverage
```

## Author

👤 **Nadav Tal**

* Website: www.nadavTal.com
* Github: [@nadavvtal](https://github.com/nadavvtal)
* LinkedIn: [@Nadav Tal](https://linkedin.com/in/nadav-tal-6b9b4b183/)

## Show your support


