module.exports = {
    extension: ["ts"],
    spec: "tests/**/*.spec.ts",
    require: "@swc-node/register",
    timeout: "100000"
  };