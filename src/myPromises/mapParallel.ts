import log from "@ajar/marker";
export async function mapP(
    promiseArr: Iterable<any> | Promise<Iterable<any>>,
    iterableFunciton: (a: any) => any | Promise<any>
) {
    try {
        if (
            typeof promiseArr === "undefined" ||
            typeof iterableFunciton === "undefined"
        ) {
            throw new Error("mapP expects 2 arguments");
        }
        log.green("Start running");
        promiseArr = await promiseArr;
        promiseArr = Array.from(promiseArr);

        const iterateArr = (promiseArr as Array<any>).map(async function (
            promise
        ) {
            await promise;
            return iterableFunciton(promise);
        });
        const resolvedArr = await Promise.all(iterateArr);
        return resolvedArr;
    } catch (err) {
        throw new Error(err as string);
    }
}
