export async function filterS(
    promiseArr: Iterable<any> | Promise<Iterable<any>>,
    booleanFunction: (a: any) => boolean | Promise<boolean>
): Promise<any[]> {
    try {
        if (
            typeof promiseArr === "undefined" ||
            typeof booleanFunction === "undefined"
        ) {
            throw new Error("filterS expects 2 arguments");
        }
        console.log("Start running");
        promiseArr = await promiseArr;
        const resultArr = [];

        for (const promise of promiseArr) {
            const resolved = await promise;
            if ((await booleanFunction(resolved)) === true) {
                resultArr.push(resolved);
            }
        }
        return resultArr;
    } catch (err) {
        throw new Error(err as string);
    }
}
