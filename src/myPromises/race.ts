export async function race(
    promises: Iterable<any> | Promise<Iterable<any>>
): Promise<any> {
    try {
        if (typeof promises === "undefined") {
            throw new Error("race expects 1 input");
        }

        promises = await promises;
        promises = Array.from(promises);

        return await new Promise((resolve) => {
            (promises as Array<Promise<any>>).forEach(async (promise) => {
                resolve(await promise);
            });
        });
    } catch (err) {
        throw new Error(err as string);
    }
}
