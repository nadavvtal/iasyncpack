export async function filterP(
    promiseArr: Iterable<any> | Promise<Iterable<any>>,
    booleanFunction: (a: any) => boolean | Promise<boolean>
): Promise<any[]> {
    try {
        if (
            typeof promiseArr === "undefined" ||
            typeof booleanFunction === "undefined"
        ) {
            throw new Error("filterP expects 2 arguments");
        }
        promiseArr = await promiseArr;
        promiseArr = Array.from(promiseArr);
        const res = [];
        const pendingArr = (promiseArr as Array<any>).map(async (item) => {
            return booleanFunction(await item);
        });
        for (const [i, p] of pendingArr.entries()) {
            if (await p) res.push((promiseArr as Array<any>)[i]);
        }
        return res;
    } catch (err) {
        throw new Error(err as string);
    }
}
