export async function reduce(
    iterable: Iterable<any> | Promise<Iterable<any>>,
    cb: (ac: any, cur: any) => any | Promise<any>,
    initial: any
): Promise<any> {
    try {
        if (
            typeof iterable === "undefined" ||
            typeof cb === "undefined" ||
            typeof initial === "undefined"
        ) {
            throw new Error("reduce expects 3 inputs");
        }
        iterable = await iterable;
        iterable = Array.from(iterable);
        let finalAcc = (iterable as Array<any>)[0];
        if (typeof initial !== "undefined") {
            finalAcc = initial;
        }

        for (const item of iterable) {
            await item;
            finalAcc = await cb(finalAcc, item);
        }

        return finalAcc;
    } catch (err) {
        throw new Error(err as string);
    }
}
