// Implementatim of Promise delay function.
export function delay(ms: number): Promise<any> {
    try {
        if (typeof ms === "undefined") {
            throw new Error("delay expects 1 input");
        }
        if (isNaN(Number(ms))) {
            throw new Error("delay expects number");
        }
        return new Promise((resolve) => setTimeout(resolve, ms));
    } catch (err) {
        throw new Error(err as string);
    }
}
