interface myObject {
    [k: string]: any;
}
export declare function props(promiseObj: myObject | Map<any, any> | Promise<myObject | Map<any, any>>): Promise<myObject | Map<any, any>>;
export {};
