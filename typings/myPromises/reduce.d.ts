export declare function reduce(iterable: Iterable<any> | Promise<Iterable<any>>, cb: (ac: any, cur: any) => any | Promise<any>, initial: any): Promise<any>;
