export declare function mapS(promiseArr: Iterable<any> | Promise<Iterable<any>>, itrFunction: (a: any) => any | Promise<any>): Promise<any[]>;
