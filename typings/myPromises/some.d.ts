export declare function some(iterable: Iterable<any> | Promise<Iterable<any>>, num: number): Promise<any>;
